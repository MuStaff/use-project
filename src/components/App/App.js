// Style
import {StyledApp} from './styled';

// Custom Hooks
import {useState} from "react";
import {useGetPosts} from "../../utils/customHooks/useFetch";

// Components
import {Header} from "../Header/Header";
import {StyledContainer} from "../Container/styled";
import {Main} from "../Main/Main";
import {Post} from "../Post/Post";
import {Button} from "../Button/Button";

function App() {
    const [page, setPage] = useState(0);
    const [count, setCount] = useState(3);

    const {data, load, err, getData} = useGetPosts(count, page, true);

    const btnNext = () => {
        if (page >= 0) setPage(page + 1)
    }

    const btnPrev = () => {
        if (page > 0) setPage(page - 1)
    }

    const getAllPost = () => {
        setCount(0);
        setPage(0);
    }
    console.log(data)
    return (
        <StyledApp>
            <Header/>
            <StyledContainer>
                <Main>
                    <Button href='#' onClick={getAllPost}>Get all posts</Button>

                    <div>
                        <Button href='#' onClick={btnPrev}>Prev</Button>
                        <a> {`< ${page + 1} >`} </a>
                        <Button href='/sf' onClick={btnNext}>Next</Button>
                    </div>

                    {data.map(post => <Post children={post} key={post._id}/>)}
                </Main>
            </StyledContainer>

        </StyledApp>
    );
}

export default App;
