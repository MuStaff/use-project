import styled from "styled-components";

export const StyledNavigate = styled.nav`
  display: flex;
  justify-content: space-between;
`